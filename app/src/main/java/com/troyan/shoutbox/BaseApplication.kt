package com.troyan.shoutbox

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid

class BaseApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
    }
}