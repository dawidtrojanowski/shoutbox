package com.troyan.shoutbox

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.troyan.shoutbox.Fragments.SettingsFragment
import com.troyan.shoutbox.Fragments.ShoutBoxFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import android.app.Activity
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    ShoutBoxFragment.OnFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener{

    private var login:String? = null
    private lateinit var mainRecyclerAdapter: MainAdapter
    private val messageList = ArrayList<Message>()
    private lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pref = getSharedPreferences(LOGIN, Context.MODE_PRIVATE)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setupViews()
        if (verifyAvailableNetwork()) {
            setupMainFragment()
        }
    }

    private fun setupViews() {
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
    }

    private fun setupMainFragment() {
        val login = pref.getString(LOGIN, null)
        if (login == null) {
            supportActionBar?.title = "Login"
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, SettingsFragment.newInstance())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        } else {
            supportActionBar?.title = "ShoutBox"
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, ShoutBoxFragment.newInstance())
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        }
    }

    fun verifyAvailableNetwork(): Boolean{
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo= connectivityManager.activeNetworkInfo
        if (!(networkInfo!=null && networkInfo.isConnected)) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }


    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.shoutBoxItem -> {
                supportActionBar?.title = "ShoutBox"
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, ShoutBoxFragment.newInstance())
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
            R.id.loginItem -> {
                supportActionBar?.title = "Login"
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, SettingsFragment.newInstance())
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d("onFragmentIntercation: ", "Loaded")
    }
}
