package com.troyan.shoutbox


data class Message(
    var content: String,
    val date: String,
    val id: String,
    val login: String
)