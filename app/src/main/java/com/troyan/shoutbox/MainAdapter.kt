package com.troyan.shoutbox

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_layout.view.*
import org.joda.time.DateTime.parse
import org.joda.time.format.DateTimeFormat

class MainAdapter(private val messageList: List<Message>) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null
    private val dateTimeFormatter = DateTimeFormat.forPattern("yyyy-mm-dd hh:mm")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.cell_layout, parent, false)
        return ViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.textViewAuthor.text = messageList[position].login
        holder.itemView.textViewContent.text = messageList[position].content
        val date = parse(messageList[position].date)
        holder.itemView.textViewDate.text = dateTimeFormatter.print(date)

        holder.itemView.setOnLongClickListener {
            onItemClickListener?.onItemClicked(messageList[position], position)
            return@setOnLongClickListener true
        }
    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface OnItemClickListener {
        fun onItemClicked(message: Message, position: Int)
    }
}