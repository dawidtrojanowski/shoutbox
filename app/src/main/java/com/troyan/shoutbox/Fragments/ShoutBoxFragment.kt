package com.troyan.shoutbox.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.troyan.shoutbox.*
import kotlinx.android.synthetic.main.fragment_shout_box.*
import kotlinx.android.synthetic.main.fragment_shout_box.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShoutBoxFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mainRecyclerAdapter: MainAdapter
    private val messageList = ArrayList<Message>()
    private val REFRESH_DELAY = 6 * 1000L
    private val threadHandler = Handler()
    private var login:String? = null
    private var pref:SharedPreferences? = null
    private var canDelete = false

    private val refreshRunnable: Runnable = object : Runnable {
        override fun run() {
            val oldMessageListSize = messageList.size
            apiServiceGetMessages()
            if (oldMessageListSize != messageList.size) {
                recycleViewMain.smoothScrollToPosition(mainRecyclerAdapter.itemCount-1)
            }
            threadHandler.postDelayed(this, REFRESH_DELAY)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        getSharedPreferences()
        setupViews()
        apiServiceGetMessages()
        startTimerThread()
        initRecycleView()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shout_box, container, false)
    }

    private fun setupViews() {
        view?.sendButton?.setOnClickListener {
            if (verifyAvailableNetwork()) {
                val content = view?.loginMessageInput?.text.toString()
                val postMessage = PostMessage(content, login!!)
                apiServicePostMessage(postMessage)
                sendButton.hideSoftKeyboard()
                loginMessageInput.setText("")
            }
        }
        setupSwipeToDelete()
    }

    private fun getSharedPreferences() {
        pref  = activity?.getSharedPreferences(LOGIN, Context.MODE_PRIVATE)
        login = pref?.getString(LOGIN, "Me").toString()
    }

    fun verifyAvailableNetwork(): Boolean{
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo= connectivityManager.activeNetworkInfo
        if (!(networkInfo!=null && networkInfo.isConnected)) {
            Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show()
            return false
        }
        return true
    }

    private fun setupSwipeToDelete() {
        val helper = ItemTouchHelper(object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }


            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (verifyAvailableNetwork()) {
                    val position = viewHolder.adapterPosition
                    if (messageList[position].login == login) {
                        apiServiceDeleteMessage(messageList[position].id, position)
                    } else {
                        Toast.makeText(context, "That's not your message", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
        helper.attachToRecyclerView(recycleViewMain)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refreshButton -> {
                if (verifyAvailableNetwork()) {
                    val oldMessageListSize = messageList.size
                    apiServiceGetMessages()
                    val sizeDifference = messageList.size - oldMessageListSize
                    if (sizeDifference == 1) {
                        Snackbar.make(activity?.findViewById(R.id.container)!!, "$sizeDifference new message", Snackbar.LENGTH_SHORT).show()
                    } else {
                        Snackbar.make(activity?.findViewById(R.id.container)!!, "$sizeDifference new messages", Snackbar.LENGTH_SHORT).show()
                    }
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun apiServiceGetMessages() {

        ApiService().getMessages().enqueue(object: Callback<List<Message>> {
            override fun onFailure(call: Call<List<Message>>, t: Throwable) {
                Log.d("onFailure: ", "Message: ${t.message}")
            }

            override fun onResponse(call: Call<List<Message>>, response: Response<List<Message>>) {
                if (response.isSuccessful) {
                    val list = response.body()
                    messageList.clear()
                    messageList.addAll(list!!.toList())
                    mainRecyclerAdapter.notifyDataSetChanged()
                    Log.d("MainActivity", "Response: ${messageList.size}")
                } else { Log.d("MainActivity", "Failure") }
            }
        })
    }

    private fun apiServicePostMessage(postMessage: PostMessage) {
        ApiService().postMessage(postMessage).enqueue(object: Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                Log.d("onFailure: ", "FailPost")
            }

            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                if (response.isSuccessful) {
                    messageList.add(response.body()!!)
                    mainRecyclerAdapter.notifyDataSetChanged()
                    recycleViewMain.smoothScrollToPosition(mainRecyclerAdapter.itemCount-1)
                } else { Log.d("MainActivity", "Failure") }
            }
        })
    }

    private fun apiServiceEditMessage(postMessage: PostMessage, id: String, position: Int) {
        ApiService().editMessage(id, postMessage).enqueue(object: Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                Log.d("onFailure: ", "FailPut")
            }

            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                if (response.isSuccessful) {
                    messageList[position].content = response.body()?.content!!
                    mainRecyclerAdapter.notifyDataSetChanged()
                }
            }
        })
    }

    private fun apiServiceDeleteMessage(id: String, position: Int) {
        ApiService().deleteMessage(id).enqueue(object: Callback<Message> {
            override fun onFailure(call: Call<Message>, t: Throwable) {
                Log.d("onFailure: ", "FailDelete: ${t}")
            }

            override fun onResponse(call: Call<Message>, response: Response<Message>) {
                if(response.isSuccessful) {
                    val result = messageList.removeAt(position)
                    mainRecyclerAdapter.notifyDataSetChanged()
                    Log.d("Result: ", result.toString())
                    Toast.makeText(context, "Message deleted", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun initRecycleView() {
        recycleViewMain.layoutManager = LinearLayoutManager(context)
        mainRecyclerAdapter = MainAdapter(messageList)
        recycleViewMain.adapter = mainRecyclerAdapter

        mainRecyclerAdapter.setOnItemClickListener(object: MainAdapter.OnItemClickListener {
            override fun onItemClicked(message: Message, position: Int) {
                if (verifyAvailableNetwork()){
                    if (message.login == login) {
                        alertDialog(message, position)
                    } else {
                        Toast.makeText(context, "This is not your message", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun alertDialog(message: Message, position: Int) {
        val builder = context?.let { AlertDialog.Builder(it) }
        builder?.setTitle(message.login)
        val input = EditText(context)

        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        builder?.setView(input)

        builder?.setPositiveButton("OK"
        ) { _, _ -> val inputText = input.text.toString()
            val postMessage = PostMessage(inputText, message.login)
            apiServiceEditMessage(postMessage, message.id,  position)
        }
        builder?.setNegativeButton("Cancel"
        ) { dialog, _ -> dialog.cancel() }
        builder?.show()
    }

    private fun startTimerThread() {
        threadHandler.postDelayed(refreshRunnable, REFRESH_DELAY)
    }





    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment shoutBoxFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            ShoutBoxFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}
